package ru.gryazev.tm.repository;

import ru.gryazev.tm.domain.Task;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepo {
    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTaskByName(String name){
        List<Task> result = new ArrayList<>();
        result = tasks.stream().filter(o ->
                o.getName().equals(name)).collect(Collectors.toList());
        return result;
    }

    public Task getTaskById(int id){
        return tasks.stream().filter(o -> o.getId() == id).findFirst().orElse(null);
    }

    public void add(Task task){
        if (task != null)
            tasks.add(task);
    }

    public boolean remove(int id){
        return tasks.removeIf(o -> o.getId() == id);
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
