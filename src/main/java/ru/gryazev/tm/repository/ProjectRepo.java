package ru.gryazev.tm.repository;

import ru.gryazev.tm.domain.Project;
import ru.gryazev.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class ProjectRepo {
    private List<Project> projects = new ArrayList<>();

    public List<Project> getProjectByName(String name){
        List<Project> result = new ArrayList<>();
        result = projects.stream().filter(o ->
                o.getName().equals(name)).collect(Collectors.toList());
        return result;
    }

    public Project getProjectById(int id){
         return projects.stream().filter(o -> o.getId() == id).findFirst().orElse(null);
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void add(Project project){
        if (project != null)
            projects.add(project);
    }

    public boolean remove(int id){
        return projects.removeIf(o -> o.getId() == id);
    }



}
