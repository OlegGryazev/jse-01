package ru.gryazev.tm.controller;

import ru.gryazev.tm.service.ProjectService;
import ru.gryazev.tm.service.TaskService;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

public class CommandController {
    private ProjectService projService;
    private TaskService taskService;
    private BufferedReader reader;

    public CommandController(BufferedReader reader) {
        projService = new ProjectService();
        taskService = new TaskService(projService.getProjectRepo());
        this.reader = reader;
    }

    public void runCommand(Command command) throws IOException{
        switch (command) {
            case PROJECT_CREATE:
                projService.create(reader);
                break;
            case PROJECT_LIST:
                projService.list();
                break;
            case PROJECT_EDIT:
                projService.edit(reader);
                break;
            case PROJECT_VIEW:
                projService.view(reader);
                break;
            case PROJECT_REMOVE:
                projService.remove(reader);
                break;
            case TASK_CREATE: taskService.create(reader);
                break;
            case TASK_VIEW: taskService.view(reader);
                break;
            case TASK_LIST: taskService.list(reader);
                break;
            case TASK_REMOVE: taskService.remove(reader);
                break;
            case TASK_EDIT: taskService.edit(reader);
                break;
            case HELP:
                Arrays.stream(Command.values()).forEach(o -> System.out.println(o.getDescription()));
                break;
            default:
                System.out.println("Command not found.");
        }
    }
}
