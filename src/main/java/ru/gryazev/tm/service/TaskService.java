package ru.gryazev.tm.service;

import ru.gryazev.tm.domain.Project;
import ru.gryazev.tm.domain.Task;
import ru.gryazev.tm.repository.ProjectRepo;
import ru.gryazev.tm.repository.TaskRepo;
import ru.gryazev.tm.util.InputUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

public class TaskService {
    private TaskRepo taskRepo;
    private ProjectRepo projectRepo;

    public TaskService(ProjectRepo projectRepo) {
        this.taskRepo = new TaskRepo();
        this.projectRepo = projectRepo;
    }

    public void create(BufferedReader in) throws IOException {
        System.out.println("[TASK CREATE]");
        int id = InputUtils.inputNumber(in, "project id");
        Project p = projectRepo.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            String name;
            do
                System.out.println("Enter task name:");
            while ("".equals(name = in.readLine()));
            taskRepo.add(new Task(name, id));
            System.out.println("[OK]");
        }
    }

    public void view(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");
        Project p = projectRepo.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            System.out.println("Enter task name:");
            String name = in.readLine();
            taskRepo.getTaskByName(name).stream().forEach(o ->
                    System.out.println(String.format("Name: %s\nDetails: %s", o.getName(), o.getDetails())));
        }
    }

    public void list(BufferedReader in) throws IOException{
        taskRepo.getTasks().stream().forEach(System.out::println);
    }

    public void edit(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepo.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            id = InputUtils.inputNumber(in, "task id");
            Task t = taskRepo.getTaskById(id);
            if (t == null)
                System.out.println("Task not found");
            else {
                String name;
                System.out.println("[TASK EDIT]");
                do
                    System.out.println("Enter new name:");
                while ("".equals(name = in.readLine()));
                t.setName(name);
                System.out.println("[OK]");
            }
        }
    }

    public void remove(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepo.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            id = InputUtils.inputNumber(in, "task id");
            if (taskRepo.remove(id))
                System.out.println("[DELETED]");
            else
                System.out.println("Task not found");
        }
    }
}