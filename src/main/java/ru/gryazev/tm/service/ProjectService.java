package ru.gryazev.tm.service;

import ru.gryazev.tm.domain.Project;
import ru.gryazev.tm.repository.ProjectRepo;
import ru.gryazev.tm.util.InputUtils;

import java.io.BufferedReader;
import java.io.IOException;

public class ProjectService{
    private ProjectRepo projectRepo;

    public ProjectService() {
        this.projectRepo = new ProjectRepo();
    }

    public void create(BufferedReader in) throws IOException {
        String name;
        System.out.println("[PROJECT CREATE]");
        do
            System.out.println("Enter name:");
        while ("".equals(name = in.readLine()));
        projectRepo.add(new Project(name));
        System.out.println("[OK]");
    }

    public void view(BufferedReader in) throws IOException {
        String result;
        System.out.println("Enter name:");
        String name = in.readLine();
        projectRepo.getProjectByName(name).stream().forEach(o ->
                System.out.println(String.format("Name: %s\nDetails: %s", o.getName(), o.getDetails())));
    }

    public void list() {
        projectRepo.getProjects().stream().forEach(System.out::println);
    }

    public void edit(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        Project p = projectRepo.getProjectById(id);
        if (p == null)
            System.out.println("Project not found");
        else {
            String name;
            System.out.println("[PROJECT EDIT]");
            do
                System.out.println("Enter name:");
            while ("".equals(name = in.readLine()));
            p.setName(name);
            System.out.println("[OK]");
        }
    }

    public void remove(BufferedReader in) throws IOException {
        int id = InputUtils.inputNumber(in, "project id");

        if (projectRepo.remove(id))
            System.out.println("[DELETED]");
        else
            System.out.println("Project not found");
    }

    public ProjectRepo getProjectRepo() {
        return projectRepo;
    }
}
