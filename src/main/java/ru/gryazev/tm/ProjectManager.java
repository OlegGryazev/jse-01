package ru.gryazev.tm;

import ru.gryazev.tm.controller.Command;
import ru.gryazev.tm.controller.CommandController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProjectManager {
    public static void main(String[] args) {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            CommandController controller = new CommandController(reader);
            String line;
            Command command;
            while (!"exit".equals(line = reader.readLine())) {
                try {
                    command = Command.valueOf(line.replace('-', '_').toUpperCase());
                } catch(IllegalArgumentException e){
                    System.out.println("Command not found!");
                    continue;
                }
                controller.runCommand(command);
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }

}
